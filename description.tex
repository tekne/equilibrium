\documentclass{article}
\usepackage[utf8]{inputenc}

\usepackage[margin=1in]{geometry}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{listings}
\usepackage{clrscode3e}

\usepackage{biblatex}
\addbibresource{references.bib}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\prt}[2]{\frac{\partial #1}{\partial #2}}

\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\argmin}{arg\,min}

\title{Initialized Equilibrium Propagation: Explained Simply}
\author{Jad Elkhaleq Ghalayini}
\date{October 2018}

\begin{document}

\maketitle

\section*{Introduction}
This document aims to explain the idea of initialized equilibrium propagation, as described in \autocite{initequib}, as simply as possible, such that the author can better understand it, and hopefully, the reader.

\section*{Idea}

\textit{Equilibrium propagation} \autocite{equibprop} is a novel algorithm for computing the gradient of an objective function, ``just like Backpropagation,'' without requiring a special computation for the ``second phase, where errors are implicitly propagated.'' \autocite{equibprop}. This makes it more biologically plausible compared to standard neural networks \autocites{equibprop}{initequib}, which are widely considered biologically implausible \autocites{equibprop}{initequib} due to the aforementioned special computation in the second phase \autocite{equibprop}.

Equilibrium propagation is an \textit{energy-based} \autocites{equibprop}{initequib} model, where the neural network is defined as a dynamical system \autocite{initequib} ``whose dynamics follow the negative-gradient of an energy function'' \autocite{initequib}, the minima of the function corresponding to a preferred state of the system \autocite{equibprop}. The network outputs as a prediction the fixed point \autocites{initequib}{equibprop} of the dynamical system described: the point at which the system reaches a local minimum energy given the input \autocite{initequib}.

The problem with equilibrium propagation is that this inference scheme makes it impractically slow for large networks, since the whole network must iteratively converge to a fixed point every training iteration \autocite{initequib}. Initialized Equilibrium Propagation aims to address this problem by ``distilling information from a slow, energy based \textit{equilibriating network} into a fast \textit{feedforward network} by training the feedforward network to predict the fixed-points of the equilibriating network with local loss'' \autocite{initequib}.

\subsection*{Equilibrium Propagation}

The following discussion is adapted from that in \autocite{initequib}.

\subsubsection*{The Model}

Let \(\mc{I}\) be the set of input neurons and \(\mc{S}\) be the set of non-input neurons. We can then define an input vector
\(\mb{x} \in \reals^{|\mc{I}|}\)
and a vector of neuron states
\(\mb{s} \in \reals^{|\mc{S}|}\)
containing one real number for each input and non-input neuron respectively.

We denote a subset \(\mc{O} \subset \mc{S}\) of the state vectors output vectors.

Denote the network architecture \(\alpha = \{\alpha_j : j \in \mc{S}\}\) where, for each \(j \in \mc{S}\),
\begin{equation}
  \alpha_j \subset
    \{i \in \mc{I} \cup \mc{S} \ | \
      i \neq j
      \land [i \in \mc{S} \implies j \in \alpha_i]
    \}
\end{equation}
is the set of neurons connected to neuron \(j\) with the conditions that
\begin{itemize}
  \item There are no self-connections, i.e. \(j \notin \alpha_j\)
  \item If neuron \(i\) is connected to neuron \(j\) and is not an input neuron, then neuron \(j\) is connected to input \(i\), i.e.
  \(i \in \alpha_j \cap \mc{S} \implies j \in \alpha_i\)
\end{itemize}
Letting \(n = |\mc{S}| + |\mc{I}|\), let \(\mb{W} \in \reals^{n \times n}\) be a weight matrix with entries only defined for \(\{w_{ij} : i \in \alpha_j\}\) with the constraint that \(w_{ij} = w_{ji}\).
Let \(\mb{b} \in \reals^{|\mc{S}|}\) be a bias vector and let \(\rho: \reals \to \reals\) be some nonlinearity.

Packaging up our parameters into \(\theta = (\mb{W}, \mb{b})\), we can now define the energy function of our model over neuron states \(\mb{s}\) and parameters \(\theta\):
\begin{equation}
  E_{\theta}(\mb{s}, \mb{x}) =
    \frac{1}{2}|\mb{s}|^2
    + \sum_{i \in \mc{S}}b_i\rho(s_i)
    - \sum_{j \in \mc{S}}\sum_{i \in \alpha_j \cap \mc{S}}w_{ij}\rho(s_i)\rho(s_j)
    - \sum_{j \in \mc{S}}\sum_{i \in \alpha_j \cap \mc{I}}x_iw_{ij}\rho(s_j)
\end{equation}
From left to right, that's
\begin{itemize}
  \item Half the squared norm of the state vector \(\mb{s}\)
  \item The dot product of the nonlinear activation of \(\mb{s}\) and the bias vector \(\mb{b}\)
  \item The product of the appropriate entries of the tensor \(\mb{W}\) with the products of the nonlinear activations of all pairs of connected state neurons
  \item The product of the appropriate entries of the tensor \(\mb{W}\) with the products of the nonlinear activations of all pairs of connected state neurons and input neurons, the input neurons \textit{not} going through nonlinear activation.
\end{itemize}
This gives the following state-dynamics for non-input neurons:
\begin{equation}
  \prt{s_j}{t}
    = -\prt{E_\theta(s, x)}{s_j}
    = -s_j
      + \rho'(s_j)\left(
        b_j
        + \sum_{j \in \mc{S}}\sum_{i \in \alpha_j \cap \mc{S}}w_{ij}\rho(s_i)
        + \sum_{j \in \mc{S}}\sum_{i \in \alpha_j \cap \mc{I}}w_{ij}x_i
      \right)
\end{equation}

\subsubsection*{Training The Model}

The network is trained in two phases: negative, and then positive. In the negative phase, we allow the network to settle to an energy minimum
\begin{equation}
  \mb{s}^- := \argmin_sE_\theta(\mb{s}, \mb{x})
\end{equation}
conditioned on a minibatch \(\mb{x}\) of input data. That is, we let \(\mb{s}\) settle to a state \(\mb{s}^-\) which minimises energy for our minibatch of input data \(\mb{x}\).

In the positive phase, we introduce a target \(\mb{y} \in \reals^{|\mb{O}|}\). We augment the energy function to ``perturb'' the fixed-point of the state towards the target with a small, positive ``clamping factor'' \(\beta \in \reals\) as follows:
\begin{equation}
  E_\theta^\beta(\mb{s}, \mb{x}, \mb{y}) =
    E_\theta(\mb{s}, \mb{x})
    + \beta\prt{C(s_{\mc{O}}, y)}{s}
\end{equation}
where \(C: \reals^{|\mc{O}|} \times \reals^{|\mc{O}|} \to \reals\) is the output loss function. We then allow the network to settle into the perturbed state
\begin{equation}
  \mb{s}^+ := \argmin_{\mb{s}}(E^\beta_\theta(\mb{s}, \mb{x}, \mb{y}))
\end{equation}
The parameters of the network are learned based on a contrastive loss between the negative and positive phase energy, which is proportional to the gradient of the output loss as \(\beta \to 0\). This is done as follows
\begin{equation}
  \Delta\theta
    = -\frac{\eta}{\beta}\left(
        \prt{E_\theta(\mb{s}^+, \mb{x})}{\theta}
        - \prt{E_\theta(\mb{s}^-, \mb{x})}{\theta}
      \right)
    \propto -\prt{C(\mb{s}_{\mc{O}}^-, \mb{y})}{\theta}
\end{equation}
where \(\eta \in \reals^+\) is the learning rate. That is, pur network parameters \(\theta\) are changed by \(\Delta\theta\), which is given by \(-\frac{\eta}{\beta}\) times the difference in the partial derivatives of the energy of the positive (\(\mb{s}^+\)) and negative (\(\mb{s}^-\)) phase fixed states with respect to \(\theta\).

This results in a local learning rule
\[\Delta w_{ij} = \frac{\eta}{\beta}(\rho(s_i^+)\rho(s_j^+) - \rho(s_i^-)\rho(s_j^-))\]
\[\Delta b_i = \frac{\eta}{\beta}(\rho(s_i^+) - \rho(s_i^-))\]
that is,
\begin{itemize}
  \item Update the \(j^{th}\) entry of the \(i^{th}\) row \(\mb{W}\) by \(\frac{\eta}{\beta}\) times the difference of the product of the activations of the \(i^{th}\) and \(j^{th}\) neurons in the positive and negative phase fixed states.
  \item Update the \(i^{th}\) entry of \(\mb{w}\) by \(\frac{\eta}{\beta}\) times the difference between the activation of the \(i^{th}\) neuron in the positive and negative fixed states.
\end{itemize}
Intuitively, we're trying to pull \(\mb{s}^- = \argmin_{\mb{s}}E_\theta(\mb{s}, \mb{x})\) as close as possible to \(\mb{s}^+ = \argmin_{\mb{s}}E_\theta^\beta(\mb{s}, \mb{x}, \mb{y})\), so as to get the network to naturally learn to minimize output loss.

The iterative settling process required to obtain \(\mb{s}^-, \mb{s}^+\) makes inference an undesirably slow process in equilibrium propagation, with the number of settling steps required scaling super-linearly with the number of layers. This is where Initialized Equilibrium Propagation steps in to attempt to provide a fast inference scheme.

\subsection*{Initialized Equilibrium Propagation}

The following discussion is adapted from that in \autocite{initequib}.

\subsubsection*{The Model}

The authors propose a feedforward network providing a function
\[f_\phi: \reals^{|\mc{I}|} \to \reals^{|\mc{S}|}, \mb{x} \mapsto \mb{s}^f\]
to predict the fixed point of the equilibriating network. We can then use the feedforward network to
\begin{itemize}
  \item Intialize the state of the equilibriating network to allow the settling process to start in the right regime
  \item Perform inference at test-time, since it learns to approximate the minimal-energy state of the equilibriating network.
\end{itemize}
In essence, we are ``distilling'' the knowledge of the slow, complex equilibrium network into a fast and simple feedforward network.

Let \(\phi = (\omega, \mb{c})\) be the parameters (weights and biases respectively) of the feedforward network, where \(\omega \in \reals^{n \times n}\) and \(\mb{c} \in \reals^{|\mc{S}|}\). We define, for each \(j \in \mc{S}\), the set
\begin{equation}
  \alpha_j^f := (i \in \alpha_j : i < j)
\end{equation}
to be the set of \textit{feedforward connections} to the neuron \(j\) (a subset of the complete set of connections) from the equilibrium network. and using this define
\begin{equation}
  \phi_j := (\omega_{\alpha_j, j} \in \reals^{|\alpha_j^f|}, c_j \in \reals)
\end{equation}
to be the set of parameters connected to the neuron \(j\). Letting \(h: \reals \to \reals\) be some nonlinear activation function, we can proceed to define \(f\) as follows:
\begin{equation}
  f_\phi(\mb{x}) = (f_{\phi_j}(x) : j \in \mc{S}) \in \reals^{\mc{S}} \in \reals^{|\mc{S}|}
\end{equation}
\begin{equation}
  f_{\phi_j}(\mb{x}) = \left\{\begin{array}{cc}
    x_j & \text{if } j \in \mc{I} \\
    h\left(
      \left(
        \sum_{i \in \alpha_j^f}\omega_{ij}f_{\phi_i}(x)
      \right)
      + c_j
    \right) & \text{if } j \in \mc{S}
  \end{array}\right. \in \reals
\end{equation}
Note that the definition for \(f_{\phi_j}\) is \textit{not} self-referential since \(f_{\phi_i}\) is only included in the sum if \(i < j\), since this is a condition for inclusion in \(\alpha_j^f\). Hence the whole ``feed\textit{forward}'' bit.

Our feedforward network \(f\) is used to produce the initial state of the negative phase of the equilibrium propagation network, so instead of starting at a zero state, we instead start at state
\[\mb{s}^f := f_\phi(\mb{x})\]
Our goal is to train the parameters \(\phi\) so as to approximate \(\mb{s}^-\) of the equilibriating network as best as possible.

Defining our loss \(\mc{L}_i\) for each neuron as follows:
\begin{equation}
  \mc{L}_i(f_{\phi_i}(\mb{x}), s_i^-) := (f_{\phi_i}(\mb{x}) - s_i^-)^2
\end{equation}
(the square of the difference between our predicted value for the \(i^{th}\) state neuron and the actual value) and from this our loss
\begin{equation}
  \mc{L}(f_{\phi}(\mb{x}), \mb{s}^-) :=
    \sum_{i \in \mc{S}}\mc{L}_i(f_{\phi_i}(\mb{x}), s_i^-)
\end{equation}
(the square of the norm of the difference between our predicticed values for the state neurons and the actual negative phase state vector)
we seek to find
\begin{equation}
  \phi^* = \argmin_\phi\mc{L}(f_\phi(\mb{x}), \mb{s}^-)
\end{equation}
(the parameters \(\phi^*\) which minimise this difference for the input \(\mb{x}\) and negative phase state vector \(\mb{s}^-\)).

We can expand the derivative of the forward parameters of the \(i^{th}\) neuron, \(\phi_i\), as follows:
\begin{equation}
  \prt{\mc{L}}{\phi_i}
    := \sum_{i \in \mc{S}}
      \prt{\mc{L}_i(f_{\phi_i}(x), s_i^-)}{\phi_i}
    = \prt{\mc{L}_i}{s_i^f}\prt{s_i^f}{\phi_i}
    + \sum_{j > i}\prt{\mc{L}_j}{s_j^f}\prt{s_j^f}{s_i^f}\prt{s_i^f}{\phi_i}
\end{equation}
We call the first term in the above sum the \textit{local} term, and the second term the \textit{distant} term. Calculating the latter is problematic, since doing so requires calculating \(\prt{s_j^f}{s_i^f}\), which requires backpropagation, defeating the entire purpose of this exercise. However, it turns out that \textit{only} optimizing the local term, which does \textit{not} require backpropagation, does not significantly affect performance.

So, over the course of training, we learn \(\phi\) until our feedforward network is a good predictor of the minimal-energy state of the equilibriating network. We can then use the feedforward network itself to perform inference, by simply taking the state of theoutput neurons as our prediction of the target data. We can take results directly, or iteratively minimize energy to refine them as we desire. However, experiments in the paper show that the forward pass performs just as well as the full energy minimzation.

Writing out the algorithm in pseudocode, we get, with step size \(\eta\), number of negative phase steps \(T^-\) and number of positive phase steps \(T^+\),
\begin{codebox}
\Procname{\(\proc{Train}(\mb{x}, \mb{y}, \epsilon, \eta, \alpha, T^-, T^+)\)}
\li \(\phi \gets \proc{InitializeFeedforwardParameters}(\alpha)\)
\li \(\theta \gets \proc{InitializeEquilibriumParameters}(\alpha)\)
\li \While not converged \Then
\li   \(\mb{x}_m, \mb{y}_m \gets \proc{SampleMinibatch}(\mb{x}, \mb{y})\)
\li   \(\mb{s}^f \gets f_\phi(\mb{x}_m)\)
\li   \(\mb{s} \gets \mb{s}\)
\li   \For \(t \in 1..T^-\) \Then
\li     \(\mb{s}
  \gets \mb{s} - \epsilon \prt{E_\theta(\mb{s}, \mb{x}_m)}{\mb{s}}\) \End
\li   \(\mb{s}^- \gets \mb{s}\)
\li   \For \(t \in 1..T^+\) \Then
\li     \(\mb{s} \gets \mb{s}
  - \epsilon\prt{E_\theta^\beta(\mb{s}, \mb{x}_m, \mb{y}_m)}{\mb{s}}\) \End
\li   \(\mb{s}^+ \gets \mb{s}\)
\li   \(\theta \gets \theta
        - \frac{\eta}{\beta} \left(
            \prt{E_\theta(\mb{s}^+, \mb{x})}{\theta}
            - \prt{E_\theta(\mb{s}^-, \mb{x})}{\theta}
      \right)\)
\li   \For \(i \in \mc{S} \cup \mc{I}\) \Then
\li     \(\phi_i \gets \phi_i
        - \eta\prt{\mc{L}_i(s_i^f, s_i^-)}{\phi_i}\) \End
\li   \End
\li \Return \(\phi, \theta\)
\end{codebox}

We can then easily define inference algorithms
\begin{codebox}
\Procname{\(\proc{FeedforwardInference}(\mb{x}, \phi)\)}
\li \(\mb{s} \gets f_\phi(\mb{x})\)
\li \Return \((s_i : i \in \mc{O})\)
\end{codebox}
\begin{codebox}
\Procname{\(\proc{IterativeInference}(\mb{x}, \phi, \theta, T^-)\)}
\li \(\mb{s} \gets f_\phi(x)\)
\li \For \(t \in 1..T^-\) \Then
\li   \(\mb{s} \gets \mb{s}
      - \epsilon\prt{E_\theta(\mb{s}, \mb{x}_m)}{\mb{s}}\) \End
\li \Return \((s_i : i \in \mc{O})\)
\end{codebox}
Question: what's \(x_m\) in \(\proc{IterativeInference}\)?

\subsubsection*{Tweaks}
The authors propose adding a loss to encourage the fixed points to stay in the regime of the forward pass. This also allows faster convergence in the negative phase, allowing learning with fewer convergence steps. Choosing a hyperparameter \(\lambda\), we define
\begin{equation}
  E_\theta^\lambda(\mb{s}, \mb{x}) = E_\theta(\mb{s}, \mb{x}) + \lambda\sum_{j \in \mc{S}}(s_j^f - s_j)^2
\end{equation}

\section*{Concrete Implementation}

TODO

\printbibliography

\end{document}
